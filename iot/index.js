'use strict';

const awsIot = require('aws-iot-device-sdk');
const uuidv4 = require('uuid/v4');
const AWS = require('aws-sdk');

AWS.config.update({
  region: "us-east-1",
  // accessKeyId default can be used while using the downloadable version of DynamoDB. 
  // For security reasons, do not store AWS Credentials in your files. Use Amazon Cognito instead.
  accessKeyId: "AKIAJYCXLOFWL5AR5ADA",
  // secretAccessKey default can be used while using the downloadable version of DynamoDB. 
  // For security reasons, do not store AWS Credentials in your files. Use Amazon Cognito instead.
  secretAccessKey: "1XIxChdWLcX+ehm7biQ0LyqAyVTWRlokRGU7lAJr"
});
const ddb = new AWS.DynamoDB({apiVersion: '2012-10-08'});

let client, iotTopic;
const IoT = { 

    connect: (topic, iotEndpoint, region, accessKey, secretKey, sessionToken) => {

        iotTopic = topic;

        client = awsIot.device({
            region: region,
            protocol: 'wss',
            accessKeyId: accessKey,
            secretKey: secretKey,
            sessionToken: sessionToken,
            port: 443,
            host: iotEndpoint
        });

        client.on('connect', onConnect);
        client.on('message', onMessage);            
        client.on('error', onError);
        client.on('reconnect', onReconnect);
        client.on('offline', onOffline);
        client.on('close', onClose);     
    },

    send: (message) => {
        client.publish(iotTopic, message);
    }  
}; 

const onConnect = () => {
    client.subscribe(iotTopic);
    addLog('Connected');
};

const onMessage = (topic, message) => {
    addLog(message);
};

const onError = () => {};
const onReconnect = () => {};
const onOffline = () => {};

const onClose = () => {
    addLog('Connection failed');
};

$(document).ready(() => {

    // initial state
    $('#btn-keys').prop('disabled', false);
    $('#btn-connect').prop('disabled', true);
    $('#btn-start').prop('disabled', true);
    $('#btn-stop').prop('disabled', true);
    $('#btn-run').prop('disabled', true);
    $('#btn-pass').prop('disabled', true);
    $('#btn-reset').prop('disabled', true);


    let iotKeys;

    $('#btn-keys').on('click', () => {
        $.ajax({
            url: window.lambdaEndpoint,
            success: (res) => {
                addLog(`Endpoint: ${res.iotEndpoint}, 
                        Region: ${res.region}, 
                        AccessKey: ${res.accessKey}, 
                        SecretKey: ${res.secretKey}, 
                        SessionToken: ${res.sessionToken}`);

                iotKeys = res; // save the keys
                
                $('#btn-keys').prop('disabled', true);
                $('#btn-connect').prop('disabled', false);
            }
        });
    });  

    $('#btn-connect').on('click', () => {
        const iotTopic = '/serverless/pubsub';        

        IoT.connect(iotTopic,
                    iotKeys.iotEndpoint, 
                    iotKeys.region, 
                    iotKeys.accessKey, 
                    iotKeys.secretKey, 
                    iotKeys.sessionToken);
        
        $('#btn-connect').prop('disabled', true);
        playReset()
    });    

    $('#btn-start').on('click', () => {
        IoT.send('playStart');  
        playStart()
        writeToDynamo('playStart')
    }); 

    $('#btn-stop').on('click', () => {
        IoT.send('playStop');  
        playStop()
        writeToDynamo('playStop')
    }); 

    $('#btn-run').on('click', () => {
        IoT.send('playRun');  
        playTypeSelected()
        writeToDynamo('playRun')
    }); 

    $('#btn-pass').on('click', () => {
        IoT.send('playPass');  
        playTypeSelected()
        writeToDynamo('playPass')
    }); 

    $('#btn-reset').on('click', () => {
        IoT.send('playReset');  
        playReset()
        writeToDynamo('playReset')
    }); 


});

const writeToDynamo = (actionType) =>{
    debugger;
    var d = new Date();
    var n = d.toISOString();
    var params = {
      TableName: 'GameFlow',
      Item: {
        'Id' : {"S":uuidv4()},
        'Play_Type' : {"S":actionType},
        'Created_Date' : {"S":n}
      }
    };

    debugger;
    ddb.putItem(params, function(err, data) {
      if (err) {
        console.log("Error", err);
      } else {
        console.log("Success", data);
      }
    });
}
const playTypeSelected = () => {
    $('#btn-start').prop('disabled', false);
    $('#btn-run').prop('disabled', true);
    $('#btn-pass').prop('disabled', true);
    $('#btn-stop').prop('disabled', true);
    $('#btn-reset').prop('disabled', false);
}

const playStart = () => {
    $('#btn-start').prop('disabled', true);
    $('#btn-run').prop('disabled', true);
    $('#btn-pass').prop('disabled', true);
    $('#btn-stop').prop('disabled', false);
    $('#btn-reset').prop('disabled', false);
}

const playStop = () => {
    $('#btn-start').prop('disabled', true);
    $('#btn-run').prop('disabled', false);
    $('#btn-pass').prop('disabled', false);
    $('#btn-stop').prop('disabled', true);
    $('#btn-reset').prop('disabled', false);
}

const playReset = () => {
    $('#btn-start').prop('disabled', false);
    $('#btn-stop').prop('disabled', true);
    $('#btn-run').prop('disabled', true);
    $('#btn-pass').prop('disabled', true);
    $('#btn-reset').prop('disabled', false);
}

const addLog = (msg) => {
    const date = (new Date()).toTimeString().slice(0, 8);
    $("#log").prepend(`<li>[${date}] ${msg}</li>`);
}